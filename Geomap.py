#================================================================================#
#  Geomap                                                                        #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

__author__ = 'Boisei0'

from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt
import pygeoip
from geoiptool.config import CITY_DB_PATH

class Geomap:
	def __init__(self, scale=0.5):
		self.map = Basemap(projection='eck4', lon_0=0, resolution='c')
		self.map.bluemarble(scale=scale)
		self.map.drawcoastlines()
		self.map.drawcountries()

		self.map.drawparallels(np.arange(-90.,120.,30.))
		self.map.drawmeridians(np.arange(0.,360.,60.))

		self.gi = pygeoip.GeoIP(CITY_DB_PATH)

	def addLocation(self, ip, marker='ro'):
		record = self.gi.record_by_addr(ip)
		lon, lat = record['longitude'], record['latitude']
		#calculate longitude and latitude to map coordinates
		xpt, ypt = self.map(lon, lat)
		lonpt, latpt = self.map(xpt, ypt, inverse=True)
		#plot data on map
		self.map.plot(xpt, ypt, marker=marker)

	def saveMap(self, title='Geomap', filename='geomap.png'):
		plt.title(title)
		plt.savefig(filename)
		print 'Map saved as %s' % filename
