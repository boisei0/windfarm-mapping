#/!usr/bin/env python
# -*- coding: utf-8 -*-

#================================================================================#
#                                                                                #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

import pygeoip
from config import CITY_DB_PATH

class geoipinfo:

	def __init__(self):
		self.gic = pygeoip.GeoIP(CITY_DB_PATH)

	def getCountryName(self, ip):
		record = self.gic.record_by_addr(ip)
		if record == None:
			return 'No info found'
		else:
			return record['country_name']

	def getCountryCode(self, ip):
		record = self.gic.record_by_addr(ip)
		if record == None:
			return 'No info found'
		else:
			return record['country_code']

	def getCity(self, ip):
		record = self.gic.record_by_addr(ip)
		if record == None:
			return 'No info found'
		else:
			return record['city']

	def getCountryCity(self, ip):
		if ip == '0.0.0.0':
			return 'Invalid ip'
		record = self.gic.record_by_addr(ip)
		if record == None:
			return 'No info found'
		else:
			if record['city'] is not None:
				return record['country_name'] + ', ' + record['city']
			else:
				return record['country_name']


	def locate(self, ip):
		if ip == '0.0.0.0' or ip == '127.0.0.1': 
			return 'Invalid ip'
		record = self.gic.record_by_addr(ip)
		if record == None:
			return 'No info found'
		if record['city'] == None or record['city'] == '':
			return record['country_name']
		elif record['city'] != None and record['country_name'] != None:
			return record['country_name'] + ', ' + record['city']


if __name__ == '__main__':
	geoipObj = geoipinfo()
	print geoipObj.locate('8.8.8.8')
