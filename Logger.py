#================================================================================#
#   Logger                                                                       #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

__author__ = 'Boisei0'

import os

class Logger:
	def __init__(self, logname, basepath=''):
		self.logname = logname
		self.log = ''
		self.basepath = basepath

	def append(self, line):
		self.log += str(line + '\n')
		self.saveLog()

	def saveLog(self):
		path = self.basepath + self.logname
		logfile = open(path, "w")
		try:
			logfile.write(self.log)
		except IOError as ex:
			print 'Error while writing log %s; error: %s' (self.logname, ex)
		finally:
			logfile.close()
