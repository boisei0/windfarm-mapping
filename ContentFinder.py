#================================================================================#
#  ContentFinder                                                                 #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

__author__ = 'Boisei0'

import os
from datetime import date

from bs4 import BeautifulSoup

class ContentFinder():
	def __init__(self, host, port):
		self.tmpLoaded = False
		self.soup = None
		self.host = host
		self.port = port
		if self.port == 443:
			self.useSSL = True
		else:
			self.useSSL  = False

	def kill(self):
		self.tmpLoaded = False
		self.soup = None
		os.system('rm -f temp/tmp.html')

	def getIndex(self, path='redir'):
		if self.useSSL:
			#os.system('wget https://%s/ -O temp/tmp.html --no-check-certificate > /dev/null 2>&1' % self.host)
			os.system('wget https://%s/%s -O temp/tmp.html --no-check-certificate > /dev/null 2>&1' % (self.host, path))
		else:
			os.system('wget http://%s:%s/%s -O temp/tmp.html > /dev/null 2>&1' % (self.host, self.port, path))
		self.tmpLoaded = True

	def getSoup(self):
		if self.tmpLoaded != True:
			self.getIndex()
		self.soup = BeautifulSoup(open("temp/tmp.html"))
		
	def getTitle(self):
		if self.soup == None:
			self.getSoup()
		#hardcoded fix for failing systems:
		if self.host == '193.212.166.190':
			return ''
		return str(self.soup.title.string)

	def getPower(self):
		if self.soup == None:
			self.getSoup()

		txt = self.soup.getText()
		posDateStart = txt.find('Wind Farm Handover') + 18 #hardcoded length: len('Wind Farm Handover') = 18
		if posDateStart == 17: # txt.find returns -1 on failure, -1 + 18 = 17
			return 0
		strDate = txt[posDateStart:posDateStart + 10] #hardcoded length: len('dd.mm.yyyy') = 10

		posProdStart = txt.find('Total Production') + 16 #hardcoded length: len('Total Production') = 16
		posProdEnd = txt.find(' MWh')
		strProd = txt[posProdStart:posProdEnd]

		days = self.dateDiffToday(int(strDate[6:]), int(strDate[3:5]), int(strDate[0:2]))
		prod = float(strProd) / 24 / days

		return prod

	def getTurbines(self):
		if self.soup == None:
			self.getSoup()

		txt = self.soup.getText()
		posTurbStart = txt.find('Number of Turbines') + 18 #hardcoded length: len('Number of Turbines') = 18
		posProdStart = txt.find('Total Production')
		strTurb = txt[posTurbStart:posProdStart] #for example: 4 (4)

		posRealTurbEnd = strTurb.find(' (')
		strRealTurb = strTurb[:posRealTurbEnd]

		return int(strRealTurb)

	def chkRealistic(self):
		''' Nordex has not yet employed turbines producing 10MW each. '''
		if self.getPower() / self.getTurbines() >= 10:
			return False
		else:
			return True

	def dateNumber(self, y, m, d):
		'''Based on http://alcor.concordia.ca/~gpkatch/gdate-algorithm.html
		   Algorithm by Gary Katch'''
		m = (m + 9) % 12		#mar=0, feb=11
		y = y - int(m/10)		#if Jan/Feb, year--
		datenumber = 365*y + int(y/4) - int(y/100) + int(y/400) + int((m*306 + 5)/10) + (d - 1)
		return datenumber

	def dateDiffToday(self, y, m, d):
		'''Based on http://alcar.concordia.ca/~gpkatch/gdate-algorithm.html
		   Algorithm by Gary Katch'''
		dToday = date.today()
		diff = self.dateNumber(dToday.year, dToday.month, dToday.day) - self.dateNumber(y,m,d)
		return diff

if __name__ == '__main__':
	cfObj = ContentFinder('217.36.121.30', 80)
	print cfObj.getTitle()
	print cfObj.dateDiffToday(2008, 1, 11)
	print cfObj.getPower()
	print cfObj.getTurbines()
	if cfObj.chkRealistic() == True:
		print 'Good one'
	else:
		print 'Kill it with fire!'
