# README #

## Nordex Wind Farm Mapper ##
This program will search [Shodan](http://shodanhq.com) for Nordex Wind Farms. 
The login pages of these wind farms contain production statistics. With these
stats, the production of the farms will be calculated. The systems will be
mapped using the [Maxmind GeoLite City Database](http://dev.maxmind.com/geoip/legacy/geolite) combined 
with Matplotlib Basemap to create a visual map of the wind farms. An example
can be seen [here](http://i.imgur.com/wKznzsE.png)

## License ##
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

## Prerequisites ##
* BeautifulSoup 4
* pygeoip
* shodan API
* numpy
* matplotlib
* mpl_toolkits.basemap (part of matplotlib when installed from source)
