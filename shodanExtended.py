#/!usr/bin/env python
# -*- coding: utf-8 -*-

#================================================================================#
#   ShodanExtended                                                               #
#   Author: Boisei0								 #
#   This program makes heavy use of the ShodanHQ API with the following license: #
#										 #
# Copyright (c) 2010 John Matherly <jmath@shodanhq.com>				 #
# 										 #
# Permission is hereby granted, free of charge, to any person			 #
# obtaining a copy of this software and associated documentation		 #
# files (the "Software"), to deal in the Software without			 #
# restriction, including without limitation the rights to use,			 #
# copy, modify, merge, publish, distribute, sublicense, and/or sell		 #
# copies of the Software, and to permit persons to whom the			 #
# Software is furnished to do so, subject to the following			 #
# conditions:									 #
# 										 #
# The above copyright notice and this permission notice shall be		 #
# included in all copies or substantial portions of the Software.		 #
# 										 #
# Except as contained in this notice, the name(s) of the above			 #
# copyright holders shall not be used in advertising or otherwise		 #
# to promote the sale, use or other dealings in this Software			 #
# without prior written authorization.						 #
# 										 #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,		 #
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES		 #
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND			 #
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT			 #
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,			 #
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING			 #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR			 #
# OTHER DEALINGS IN THE SOFTWARE.						 #
#--------------------------------------------------------------------------------#
#  ShodanExtended uses the GNU General public License				 #
#										 #
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

import httplib, base64, socket, os, time
from shodan import WebAPI

class ShodanExtended:
	def __init__(self, apiKey):
		self.api = WebAPI(apiKey)

	def initHeaders(self, uname, passw):
		self.uname = uname
		self.passw = passw
		auth = base64.encodestring("%s:%s" % (self.uname, self.passw))
		self.headers = {"Authorization" : "Basic %s" % auth}

	def search(self, query, page=1, limit=None, offset=None):
		try:
			result = self.api.search(query, page, limit, offset)
			return result
		except Exception as ex:
			print 'Exception in ShodanExtended.search: %s' % str(ex)
			return 'Error: %s' % str(ex)

	def count(self, query):
		try:
			result = self.api.count(query)
			msg = 'Found %s results.'% str(result['total'])
			return result['total']
		except Exception as ex:
			print 'Exception in ShodanExtended.count: %s' % str(ex)
                        return 'Error: %s' % str(ex)

	def hostLookup(self, ip):
		if ip == '127.0.0.1':
			return 'No info found.'
		try:
			hostinfo = self.api.host(ip)
			serviceinfo = ''
			for item in hostinfo['data']:
				serviceinfo += str(item['port']) + ', '
			msg = '%s is running services on the following ports: %s' % (ip, serviceinfo)
			return msg
		except Exception as ex:
			print 'Exception in ShodanExtended.hostLookup: %s' % str(ex)
			return 'Error (Exception in ShodanExtended.hostLookup): %s' % str(ex)

	def chkSiteUp(self, ip, port=80, defUname=None, defPassw=None, timeout=4, path=''):
		try:
			if defUname == None and defPassw == None:
				self.headers=None
			elif defUname == None and defPassw != None:
				initHeaders(defUname, '')
			elif defUname != None and defPassw == None:
				initHeaders('', defPassw)
			else:
				initHeaders(defUname, defPassw)
			conn = None

			if port == 443:
				conn = httplib.HTTPSConnection(ip, port, timeout=timeout)
			else:
				conn = httplib.HTTPConnection(ip, port, timeout=timeout)
			if self.headers == None:
				conn.request("GET", "/%s" % path)
			else:
	                        conn.request("GET", "/%s" % path, headers=self.headers)
                        resp = conn.getresponse()
                        return True
                except socket.timeout:
                        return False
                except socket.error:
                        return False
                except httplib.BadStatusLine:
                        return False		

	def ping(self, ip):
		try:
			rpl = os.system('ping %s -c 1 -q' % ip)
			return rpl
		except Exception as ex:
			print 'Error in ShodanExtended.ping: %s' % str(ex)
			return 'Error in ShodanExtended.ping: %s' % str(ex)
