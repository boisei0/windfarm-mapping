#================================================================================#
#  Nordex.py	                                                                 #
#  Mapping Nordex Wind Farms							 #
#  Logs statistics from the login page, as well as saving a map of the found     #
#  wind farms.									 #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

__author__ = 'Boisei0'

import time

from shodanExtended import *
from ContentFinder import *
from Logger import *
from Geomap import *

class Nordex:
	def __init__(self):
		#Config
		SHODANAPI_KEY = ''

		self.logObj = Logger('Nordex-%s.log' % time.time())
		self.shodanObj = ShodanExtended(SHODANAPI_KEY)
		self.blacklist = self.importBlacklist()
		self.upUnique = 0
		self.power = 0
		self.ipKnown = []

	def importBlacklist(self):
		blacklist = []
		try:
			with open('blacklist.dat', 'a+') as f:
				strBlacklist = str(f.readlines())
				blacklist = strBlacklist.split()
		except IOError as ex:
			print 'Error importing blacklist: %s' % ex
		finally:
			return blacklist

	def appendBlacklist(self, entry):
		blacklist = []
		with open('blacklist.dat', 'a') as f:
                        f.write('%s\n' % entry)

	def initLog(self):
		self.logObj.append('Searching for Nordex Control 2 systems, running Jetty/3.1.8 (Windows 2000 5.0 x86)')
		self.logObj.append('Unrealistic systems are blacklisted and will not show up in this log')
		self.logObj.append('Results:')

	def finLog(self):
		self.logObj.append('[*] Unique systems up: %s' % self.upUnique)
		self.logObj.append('[*] Total production: %s MW' % self.power)

	def main(self):
		gmObj = Geomap()
		query = 'Jetty/3.1.8 (Windows 2000 5.0 x86) "200 OK"'
		qryShort = 'Jetty/3.1.8'
		pages = self.shodanObj.count(query) / 100 + 1

		self.initLog()

		for i in range (1,pages):
			result = self.shodanObj.search(query, page=i)
			for host in result['matches']:
				print '%s:%s' % (host['ip'], host['port'])

				if host['ip'] in self.blacklist:
					print 'Blacklisted ip detected; should stop now; ip: %s:5s' % (host['ip'], host['port'])
				elif self.shodanObj.chkSiteUp(host['ip'], host['port'], path='redir') == True:
					if host['ip'] not in self.ipKnown:
						cfObj = ContentFinder(host['ip'], host['port']);
						try:
							if cfObj.getPower() == 'mber':
								#Non default page -> blacklist
								raise ValueError
							if cfObj.getPower() == 0:
								#No statistics available -> blacklist
								raise ValueError
							if cfObj.chkRealistic() == False:
								#Production statistics are not realistic -> blacklist
								raise ValueError

							self.logObj.append('[+] %s:%s is up' % (host['ip'], host['port']))
							self.logObj.append('[*] Title: %s' % (cfObj.getTitle()))
							self.logObj.append('[*] Power: %s MW' % (cfObj.getPower()))
							self.logObj.append('')

							gmObj.addLocation(host['ip'])

							self.upUnique += 1
							self.power += cfObj.getPower()
						except ValueError:
							self.appendBlacklist(host['ip'])
						finally:
							self.ipKnown.append(host['ip'])
							cfObj.kill()
		self.finLog()
		gmObj.saveMap('Nordex Wind Farms', 'nordex.png')

if __name__ == '__main__':
	nordexObj = Nordex()
	nordexObj.main()
